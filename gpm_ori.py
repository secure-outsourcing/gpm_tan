#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Implementation of GPM operation.
'''

__author__ = 'Tan Li'

import misc
import numpy as np


class gpm_ori(object):
    '''
    Attributes:
            f (function): objective function, goal is to minimize it.
            gs (array_like): a list of inequality constaints functions, some of them could become active
            bs (array_like): a list of real number bounds for gs.
            hs (array_like): a list of equality constaints functions, they are active at all time
            ds (array_like): a list of real number bounds for hs.
            x (numpy.ndarray): a vector represents a point, will be updated at each iteration in self.calculate().
            activeGs (array_like): a list of indices of active inequality constaints functions in self.gs.
            activeBs (array_like): a list of indices of real number bounds for active constraints in self.bs.
            accuracy (float): when a number is smaller than accuracy we consider it's close enough to zero
            activeThreshold (float): when the point is within this threshold to a constraint bound, consider this contraint active
    '''

    def __init__(self, f, gs, bs, hs, ds, x0, accuracy=0.001, activeThreshold=0.01):
        '''Constructor of GPM.

        Args:
            f (function): objective function, goal is to minimize it.
            gs (array_like): a list of inequality constaints functions.
            bs (array_like): a list of real number bounds for gs.
            hs (array_like): a list of equality constaints functions.
            ds (array_like): a list of real number bounds for hs.
            x0 (numpy.ndarray): initial guess, a vector represents a point, must satisfies all equality constaints.
            accuracy (float): when a number is smaller than accuracy we consider it's close enough to zero
            activeThreshold (float): when the point is within this threshold to a constraint bound, consider this contraint active

        Raises:
            valueError: If the number of constraints and constraint bounds don't match or initial guess is not legal.
        '''

        self.f = f
        self.gs = gs
        self.bs = bs
        self.hs = hs
        self.ds = ds
        self.x = x0
        self.accuracy = accuracy
        self.activeThreshold = activeThreshold

        # check Args:, raise valueError if not pass
        if self.checkArgs() == False:
            print("initialization stopped.")
            raise ValueError("not a good start.")

        print("initialization finished.")

        # active constraints are to be determinted
        self.activeGs = []
        self.activeBs = []

    def checkArgs(self):
        '''Check if all args are legal.

        Returns:
            bool: True if all legal, False otherwise.
        '''

        if len(self.gs) != len(self.bs) and not self.gs and not self.bs:
            print('number of inequality constraints and bounds don\'t match.')
            print(f"{len(self.gs)},{len(self.bs)}")
            return False

        if len(self.hs) != len(self.ds) and not self.hs and not self.ds:
            print('number of equality constraints and bounds don\'t match.')
            print(f"{len(self.hs)},{len(self.ds)}")
            return False

        for h, d in zip(self.hs, self.ds):
            if h(*self.x) > d or d - h(*self.x) > self.accuracy:
                print("initial guess doesn't satisfy equality constraints")
                return False

        for g, b in zip(self.gs, self.bs):
            if g(*self.x) > b:
                print("initial guess doesn't satisfy inequality constraints")
                return False

        return True

    def updateActive(self, x):
        '''Update active constraints based on given point x.
        Args:
            x (numpy.ndarray): a vector represents a point.
        '''
        self.activeGs = []
        self.activeBs = []

        for i in range(len(self.gs)):
            if self.bs[i] - self.gs[i](*x) <= self.activeThreshold:
                self.activeGs.append(i)
                self.activeBs.append(i)
        print(
            f"{len(self.activeGs)} active inequality constraints out of {len(self.gs)}")

    def ifViolate(self, x):
        for i in range(len(self.gs)):
            if self.gs[i](*x) - self.bs[i] > 0:
                return True

        for i in range(len(self.hs)):
            if self.hs[i](*x) - self.ds[i] > 0 or self.ds[i] - self.hs[i](*x) > self.accuracy:
                return True

        return False

    def getGx(self, x):
        '''Get the gap between the values of active constraint functions and the corresponding constants.

        Args:
            x (numpy.ndarray): a vector represents a point.
        '''
        __res = []
        for i in self.activeGs:
            __res.append(self.gs[i](*x)-self.bs[i])

        for h, d in zip(self.hs, self.ds):
            __res.append(h(*x)-d)

        return np.array(__res)

    def calculate(self, reduction=0.01):
        '''start gpm loop, final result saved to self.x

        Args:
            reduction (float)): the desired specified reduction, reduction ~=( f(Xk)-f(Xk+1) )/ f(Xk)
        '''

        numOfVariables = len(self.x)

        x = self.x
        update = True
        iteration = 0
        while True:
            iteration += 1
            print(f"iteration {iteration}:")
            print(f"x: {x}")
            print(f"f(x): {self.f(*x)}")

            if update:
                self.updateActive(x)
            else:
                update = not update

            gradientF = misc.gradient(self.f, x)
            # print(f"gradientF:\n{gradientF}")

            gradientOfConstraints = []

            for i in range(len(self.activeGs)):
                gradientOfConstraints.append(
                    misc.gradient(self.gs[self.activeGs[i]], x))

            for i in range(len(self.hs)):
                gradientOfConstraints.append(misc.gradient(self.hs[i], x))

            # print(f"self.gs[0](*x): {self.gs[0](*x)}")
            # print(f"self.gs[self.activeGs[0]](*x): {self.gs[self.activeGs[0]](*x)}")
            # print(f"first gradient of active constraints:\n{misc.gradient(self.gs[0], x)}")
            # print(f"gradientOfConstraints:\n{gradientOfConstraints}")
            # N is the gradient matrix of active constraints
            if gradientOfConstraints:
                N = np.column_stack(tuple(gradientOfConstraints)).T
                # P is the projection matrix

                P = np.identity(numOfVariables) - \
                    N.T @ np.linalg.inv(N @ N.T) @ N

                gx = self.getGx(x)

                # M0 = N @ gradientF
                # M1 = np.linalg.inv(N @ N.T)

                Y = np.dot(P @ gradientF, gradientF) / (2*reduction)

                Mu1 = -np.linalg.inv(N @ N.T) @ N @ gradientF
                Mu2 = -np.linalg.inv(N @ N.T) @ gx

                Mu = Mu1 + 2 * Y * Mu2

                MuMin = np.amin(Mu)
                print(f"Min of multipliers: {MuMin}")
                if MuMin >= 0:
                    dx1 = gradientF + N.T @ Mu1
                    dx2 = -N.T @ Mu2

                    dx = -1/(2*Y) * dx1 + dx2

                    x = x + dx

                    if np.linalg.norm(dx1) <= self.accuracy:
                        break

                else:
                    update = False
                    MuMinIndex = np.where(Mu == MuMin)[0][0]
                    print(f" MuMinIndex: {MuMinIndex}")
                    if MuMinIndex < len(self.activeGs):
                        print(
                            f"Number {self.activeGs[MuMinIndex]} active inequality constraints is removed.")
                        self.activeGs.pop(MuMinIndex)
                        self.activeBs.pop(MuMinIndex)
                    else:
                        print(
                            f"Number {MuMinIndex - len(self.activeGs)} equality constraints is removed.")
                        self.hs.pop(MuMinIndex - len(self.activeGs))
                        self.ds.pop(MuMinIndex - len(self.activeGs))

            else:
                P = np.identity(numOfVariables)
                Y = np.dot(P @ gradientF, gradientF) / (2*reduction)
                dx = -1/(2*Y) * gradientF
                x = x + dx

            
            

        print(f"final result x* : {x}, f(x*) : {self.f(*x)}")
        self.x = x

        # print(f"{self.f(*np.array([0,1,2,-1]))}")
