import sys
import dask.array as da
import dask.array.random as rand
from dask.delayed import delayed
from dask.array.linalg import inv
from secout.utils import generate_permutation_matrix
from secout.base import CloudAlgorithm

class DaskQP(CloudAlgorithm):
    """Secure Quadratic Programming solver

    See Also
    --------
    dask.distributed.Client
    """
    def __init__(self, Q, b, A, c, remote=None, local=None):
        # currently we consider inputs are normal numpy arrays
        super().__init__(local, remote)

        assert Q.shape[0] == Q.shape[
            1], "Incorrect dimension for quadratic coefficient"
        assert A.shape[1] == Q.shape[
            1], "Affine constraint and coefficient dimension mismatch"
        assert Q.shape[0] == b.shape[
            0], "Quadratic and linear coefficient dimension mismatch"
        assert A.shape[0] == c.shape[0], "Affine constraints dimension mismatch"

        for k in ["Q", 'b', 'A', 'c']:
            var = locals()[k]
            shape = var.shape
            setattr(self, k,
                    da.from_delayed(delayed(var), shape=shape, dtype=float))

    def dual_formulation(self):
        Q_bar, V_Q, T_Q = self.secure_mask(self.Q)
        A_bar, V_A, _ = self.secure_mask(self.A, T=T_Q)

        print(Q_bar.shape, V_Q.shape, T_Q.shape, A_bar.shape)

        Q_bar = da.from_array(self.client.compute(Q_bar).result())
        A_bar = da.from_array(self.client.compute(A_bar).result())

        # compute W_bar at cloud
        W_bar = A_bar @ inv(Q_bar)
        W_bar = self.cloud.compute(W_bar).result()

        W = inv(V_A) @ W_bar @ V_Q
        self.W = W  # DEBUG, W is correct

        Wp_bar, V_W, T_W = self.secure_mask(W)
        Ap_bar, _, V_AT = self.secure_mask(self.A.transpose(),
                                           V=inv(T_W))
        Wp_bar = da.from_array(self.client.compute(Wp_bar).result())
        Ap_bar = da.from_array(self.client.compute(Ap_bar).result())

        # compute P_bar at the cloud
        P_bar = Wp_bar @ Ap_bar
        P_bar = da.from_array(self.cloud.compute(P_bar).result())

        self.r = self.c - W @ self.b
        self.P = inv(V_W) @ P_bar @ inv(V_AT)

    def secure_mask(self, H, V=None, T=None):
        if V is None:
            V = generate_permutation_matrix(H.shape[0]) @ \
                da.diag(rand.uniform(
                    - sys.maxsize,
                    sys.maxsize,
                    size=(H.shape[0],)
                ))

        if T is None:
            T = da.diag(rand.uniform(
                sys.float_info.epsilon,
                sys.maxsize,
                size=(H.shape[1],)
            )) @ generate_permutation_matrix(H.shape[1])

        H_bar = V @ H @ T
        return H_bar, V, T
