import abc

class BaseTest(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def do(cls, *args, **kwargs):
        pass
