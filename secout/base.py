from dask.distributed import Client


class CloudAlgorithm:
    def __init__(self, local, remote):
        self.cloud = remote if remote is not None else Client()
        self.client = local if local is not None else Client()
