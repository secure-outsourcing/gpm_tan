import numpy as np


def generate_permutation_matrix(size: int):
    """
    Obtain permutation matrix
    :param size: the size of the permutation matrix
    :return: permutation matrix
    """

    r = np.zeros((size, size), dtype=int)
    order = np.random.permutation(size)
    for j, i in enumerate(order):
        r[i, j] = 1
    return r
