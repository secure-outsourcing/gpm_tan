import warnings

import dask.array as da
from dask.array import random as rand
from dask.array import transpose
from dask.array.linalg import norm

from secout.base import CloudAlgorithm
from secout.utils import generate_permutation_matrix


# noinspection PyPep8Naming
class DaskLSE(CloudAlgorithm):
    """Secure solver for linear system of equation.

    Solves the specified linear system of equation by exploiting local and remote
    computational resources using the proposed algorithms.

    Parameters
    ----------
    A: (M, N) array_like
        The coefficient matrix of the linear system to be solved
    b: (M,) matrix_like
        The constant matrix of the linear system to be solved
    l: int, optional
        Parameter for additive noise, assuming that the value of A is within
        (-2^l, 2^l) (the default is 16)
    p: int, optional
        parameter for additive noise (the default is 8).
    q: int, optional
        parameter for additive noise (the default is 4).
    remote: dask.distributed.Client
        The client instance represents remote resources
    local: dask.distributed.Client
        The client instance represents local resources

    Attributes
    ----------
    Ap : (N, N) array_like
        Transformed coefficient matrix
    bp : (N, ) array_like
        Transformed constant matrix
    x : (N,) array_like
        Solution for the input problem

    See Also
    --------
    dask.distributed.Client
    """

    def __init__(self, A, b, l=16, p=6, q=3, remote=None, local=None):
        # problem checks
        assert len(A.shape) == 2, "A has invalid dimension %s" % A.shape
        assert len(b.shape) == 1, "b has invalid dimension %s" % b.shape
        assert A.shape[0] == b.shape[0], "A.shape[0] == b.shape[0], yet " + \
            "%d != %d" % (A.shape[0], b.shape[0])

        # instantiate super class
        super().__init__(local, remote)

        # TODO: expand to HDF5
        # currently we consider inputs are normal numpy arrays
        self.A = da.from_array(A)
        self.b = da.from_array(b)
        self.__A = self.A
        self.__b = self.b

        self.l = l
        self.p = p
        self.q = q

        self.Ap = None
        self.bp = None
        self.x = None

        if A.max() > 2 ** l or A.min() < -2 ** l:
            warnings.warn(
                "The range of the problem exceeds the noise to be added" +
                "to the system. Consider normalize the problem with " +
                "`solver.normalize_problem()` to avoid compromised privacy.",
                RuntimeWarning
            )

    def normalize_problem(self):
        """Normalize the coefficient matrix `A` and constant matrix `b`

        The normalization is based on the upper and lower bound of A.

        Returns
        -------
        None
        """
        lower = self.__A.min()
        upper = self.__A.max()
        self.A = self.__A / (upper - lower)
        self.b = self.__b / (upper - lower)

    def denormalize_problem(self):
        """Recover coefficient `A` and constant `b`  to de-normalized form

        Returns
        -------
        None
        """
        self.A = self.__A
        self.b = self.__b

    def secure_mask(self, A, Q=None, P=None, Z=None):
        """Perform secure masking `Q @ (A + Z) @ P`.

        Parameters
        ----------
        A: (M, N) array_like
            The array to be masked
        Q: (M, M) array_like, optional
            Row permutation matrix. If Q is not supplied, a random permutation
            matrix of corresponding shape is generated.
        P: (N, N) array_like, optional
            Column permutation matrix. If P is not supplied, a random permutation
            matrix of corresponding shape is generated.
        Z: (M, N) array_like, optional
            Additive noise matrix. If Z is not supplied, a random additive
            noise of ``A.shape`` is generated

        Returns
        -------
        (M, N) array_like
            The result of the secure mask ``Q @ (A + Z) @ P``
        Z: (M, N) array_like
            Additive noise
        Q: (M, M) array_like
            Row permutation matrix
        P: (N, N) array_like
            Column permutation matrix
        """
        l = self.l
        p = self.p
        q = self.q
        if Q is None:
            Q = da.from_array(
                generate_permutation_matrix(A.shape[0])).transpose()
        if P is None:
            P = da.from_array(
                generate_permutation_matrix(A.shape[1])).transpose()
        if Z is None:
            u = rand.uniform(-2. ** p, 2. ** p, size=A.shape[0])
            v = rand.random(size=A.shape[1]) * (
                    2. ** (l + q) - 2. ** l) + 2. ** l
            Z = da.outer(u, v)
        return Q @ (A + Z) @ P, Z, Q, P

    def slse_transformation(self):
        """Performs SLSE transformations to the given problem.

        Computes :math:`A.T A` and :math:`A.T b` using the proposed secure LSE
        transformation. The results are saved as instance attributes `Ap` and
        `bp`.

        Returns
        -------
        None
        """
        self.Ap, self.bp = self._slse_transformation(self.A, self.b)

    def _slse_transformation(self, A, b):
        """Performs SLSE Transformation to the input problem.

        Computes :math:`A.T A` and :math:`A.T b` using the proposed secure LSE
        transformation.

        Parameters
        ----------
        A: (M, N) matrix_like
            Coefficient matrix to the linear system of equations
        b : (M,) matrix_like
            Constant matrix to the linear system of equations

        Returns
        -------
        Ap: (N, N) ndarray
            The transformed coefficient matrix. :math:`A.T A`
        bp: (N,) ndarray
            The transformed constant matrix. :math:`A.T b`
        """
        # compute Ap = A^T @ A
        T_0 = da.from_array(generate_permutation_matrix(A.shape[1]))
        Ah_0, Zt_0, Q_0_T, P_0_T = self.secure_mask(A)
        Ah_1, Zt_1, _, _ = self.secure_mask(A, Q=Q_0_T, P=T_0)
        Ah_0 = da.from_array(self.client.compute(Ah_0).result())
        Ah_1 = da.from_array(self.client.compute(Ah_1).result())

        # compute G at the cloud
        G = Ah_0.transpose() @ Ah_1
        G_fu = self.cloud.compute(G)  # Async @ cloud

        M = Zt_0.transpose() @ A + \
            A.transpose() @ Zt_1 + \
            Zt_0.transpose() @ Zt_1

        G = da.from_array(G_fu.result())

        Ap = P_0_T @ G @ T_0.transpose() - M
        bp = A.transpose() @ b

        return Ap, bp

    def ppcgm(self, tol=1e-5):
        """Performs Privacy Preserving Conjugate Gradient Method (PPCGM).

        The result is saved as a instance attribute `x`.

        Parameters
        ----------
        tol: float, optional
            The stopping threshold for PPCGM (default is `1e-5`)

        Returns
        -------
        None
        """
        self.x = self._ppcgm(self.Ap, self.bp, tol)

    def _ppcgm(self, Ap, bp, tol):
        """Performs Privacy Preserving Conjugate Gradient Method (PPCGM).

        Computes the result of the input linear system using the proposed PPCGM.
        The input linear system must be non-singular, symmetric and positive
        definite.

        Parameters
        ----------
        Ap: (N, N) array_like
            Coefficient matrix that is non-singular, symmetric and positive
            definite.
        bp: (N,) array_like
            Constant matrix
        tol: float, optional
            The stopping threshold for PPCGM (default is `1e-5`)

        Returns
        -------
        x : (N,) array_like
            The solution to the linear system computed by PPCGM
        """
        Ap_h, Zt, P, T = self.secure_mask(Ap)
        Ap_h = da.from_array(self.client.compute(Ap_h).result())

        # Initial guess is zero
        x = da.zeros((Ap.shape[0],))
        x_h = transpose(T) @ x
        x_h = da.from_array(self.client.compute(x_h).result())

        h0 = Ap_h @ x_h
        h0 = da.from_array(self.cloud.compute(h0).result())

        r = P.transpose() @ h0 - Zt @ x - bp
        p = -r
        rho = transpose(r) @ r

        delta = self.client.compute(
            (tol * norm(bp))**2
        ).result()
        print("Initial rho: %8f" % rho)
        print("Target rho : %8f"% delta)

        i = 0
        while rho > delta:
            ph1_T = transpose(p) @ transpose(P)
            ph1_T = da.from_array(self.client.compute(ph1_T).result())

            ph2 = transpose(T) @ p
            ph2 = da.from_array(self.client.compute(ph2).result())

            f = Ap_h @ ph2
            f = da.from_array(self.cloud.compute(f).result())

            t = ph1_T @ Ap_h @ ph2
            t = da.from_array(self.cloud.compute(t).result())

            alpha = rho / (t - transpose(p) @ Zt @ p)

            r = r + alpha * (transpose(P) @ f - Zt @ p)
            rho_old = rho
            rho = self.client.compute(transpose(r) @ r).result()

            x = x + alpha * p
            beta = rho / rho_old
            p = -r + beta * p

            if i < 50 or i % 691 == 0:
                print("iteration {:d} rho {:8f}".format(i, rho))
            i += 1
            if i > 100000:
                break
        print("Exited at iteration {:d} rho {:8f}".format(i, rho))
        return x

    def solve(self, tol=1e-5):
        """
        Solve any linear system of equations using the proposed algorithm.

        Transform the input `A` and `b` using SLSE_transformation and compute
        the result using PPCGM.

        Parameters
        ----------
        tol: float, optional
            The stopping threshold for PPCGM (default is `1e-5`)

        Returns
        -------
        None
        """
        self.slse_transformation()
        self.ppcgm(tol=tol)
