from __future__ import absolute_import

import pytest

pytest.importorskip("numpy")
pytest.importorskip("dask")

from dask.distributed import LocalCluster, Client
from dask.array.utils import allclose, assert_eq
import dask.array as da
import numpy as np
import itertools
from ..LSE import DaskLSE as SLSE

@pytest.fixture(scope='module', autouse=True)
def remote_client():
    """
    A client that represent the remote computational resources; runs on a local
    cluster
    Scope is `module` to reduce instantiation time
    """
    client = Client(n_workers=2, threads_per_worker=1)
    print("\nInstantiating remote client\n")
    yield client
    client.close()


@pytest.fixture(scope='module', autouse=True)
def local_client():
    """
    A client that represents local computational resources
    """
    client = Client(n_workers=2, threads_per_worker=1)
    print("\nInstantiating local client\n")
    yield client
    client.close()


ONE_SIMPLE_CASE = (
    np.asarray([
        [1, -1, 3],
        [2, 1, 0],
        [-1, -5, 9]
    ]),
    np.asarray(
        [1, 5, -7]
    )
)

__CHUNKS = [
    da.arange(11, 30),
    da.arange(70, 150),
    da.arange(100, 800),
]

TEST_CASES = list(
    itertools.starmap(
        lambda a, b: (np.outer(a, b) + 1, a),
        itertools.permutations(__CHUNKS, 2)
    )
)

del __CHUNKS

class TestSLSETransformation:

    def test_1(self, remote_client, local_client):
        A,b = ONE_SIMPLE_CASE

        solver = SLSE(A, b)
        solver.slse_transformation()

        assert_eq(A.T @ A, solver.Ap)
        assert_eq(A.T @ b, solver.bp)


def test_1(remote_client, local_client):
    """
    One fixed and simple example; test that SLSE solver successfully computes
    result of a given linear system of equations
    :param remote_client: remote client fixture for the cloud resources
    :param local_client: local client fixture for the local resources
    """
    A, b = ONE_SIMPLE_CASE
    dA, db = da.asarray(A), da.asarray(b)
    solver = SLSE(dA, db, remote=remote_client, local=local_client)
    solver.solve()

    cb = remote_client.compute(dA @ solver.x, sync=True)

    allclose(cb, b)





def __run_and_test(A, b, remote_client, local_client, l=8, normalize=False):
    dA, db = da.asarray(A), da.asarray(b)
    solver = SLSE(dA, db, remote=remote_client, local=local_client, l=l)
    if normalize:
        solver.normalize_problem()
    solver.solve()
    cb = remote_client.compute(dA @ solver.x, sync=True)
    allclose(cb, b)


@pytest.mark.parametrize(
    "A,b",
    TEST_CASES
)
def test_static(A, b, remote_client, local_client):
    __run_and_test(A, b, remote_client, local_client)


@pytest.mark.parametrize(
    "m,n",
    [
        # long thin
        (10, 3),
        (30, 7),
        # wide fat
        (50, 80),
        # square
        (20, 20),
        (90, 90),
    ]
)
@pytest.mark.parametrize(
    "exp,l",
    [
        (5, 32),
        (32, 16),
    ]
)
def test_random(m, n, exp, l, remote_client, local_client):
    A = da.random.random((m, n)) * 2 ** exp - 2 ** (exp - 1)
    b = da.random.random((m,)) * 2 ** exp
    normalize = True if exp >= l else False
    __run_and_test(A, b, remote_client, local_client, l=l, normalize=normalize)
