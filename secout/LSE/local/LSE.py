import numpy as np
import numpy.random as rand
from numpy.linalg import norm
from secout.LSE.local.utils import generate_permutation_matrix


def secure_mask(A, Q=None, P=None, Z=None, l=16, p=8, q=4):
    """Perform secure masking `Q @ (A + Z) @ P`.

    Parameters
    ----------
    A : (M, N) array_like
        The array to be masked
    Q : (M, M) array_like, optional
        Row permutation matrix. If Q is not supplied, a random permutation
        matrix of corresponding shape is generated.
    P : (N, N) array_like, optional
        Column permutation matrix. If P is not supplied, a random permutation
        matrix of corresponding shape is generated.
    Z : (M, N) array_like, optional
        Additive noise matrix. If Z is not supplied, a random additive
        noise of ``A.shape`` is generated
    l : int, optional
        Parameter for additive noise, assuming that the value of A is within
        (-2^l, 2^l) (the default is 16)
    p : int, optional
        parameter for additive noise (the default is 8).
    q : int, optional
        parameter for additive noise (the default is 4).

    Returns
    -------
    (M, N) array_like
        The result of the secure mask ``Q @ (A + Z) @ P``
    Z : (M, N) array_like
        Additive noise
    Q : (M, M) array_like
        Row permutation matrix
    P : (N, N) array_like
        Column permutation matrix
    """
    if Q is None:
        Q = generate_permutation_matrix(A.shape[0]).T
    if P is None:
        P = generate_permutation_matrix(A.shape[1]).T
    if Z is None:
        u = rand.uniform(-2. ** p, 2. ** p, size=A.shape[0])
        v = rand.random(size=A.shape[1]) * (2.**(l + q)-2. ** l) + 2. ** l
        Z = np.outer(u, v)
    return Q @ (A + Z) @ P, Z, Q, P


def slse_transformation(A, b, l):
    """Performs SLSE Transformation to the input problem.

    Computes :math:`A.T A` and :math:`A.T b` using the proposed secure LSE
    transformation using local computational resources.

    Parameters
    ----------
    A : (M, N) matrix_like
        Coefficient matrix to the linear system of equations
    b : (M,) matrix_like
        Constant matrix to the linear system of equations
    l : int
        Constraint for A: The values in A must be within :math:`(-2^l, 2^l)`

    Returns
    -------
    Ap : (N, N) ndarray
        The transformed coefficient matrix. :math:`A.T A`
    bp : (N,) ndarray
        The transformed constant matrix. :math:`A.T b`

    """
    Ah_0, Zt_0, Q_0_T, P_0_T = secure_mask(A, l=l)
    Ah_1, Zt_1, _    , T_0   = secure_mask(A, Q=Q_0_T, l=l)

    G = Ah_0.T @ Ah_1

    Ap = P_0_T @ G @ T_0.T - (Zt_0.T @ A + A.T @ Zt_1 + Zt_0.T @ Zt_1)
    bp = A.T @ b
    return Ap, bp

def ppcgm(Ap, bp, l, tol=1e-5):
    """ Performs Privacy Preserving Conjugate Gradient Method (PPCGM).

    Computes the result of the input linear system using the proposed PPCGM with
    local computational resources. The input linear system must be nonsingular,
    symmetric and positive definite.

    Parameters
    ----------
    Ap : (N, N) array_like
        Coefficient matrix that is nonsingular, symmetric and positive definite.
    bp : (N,) array_like
        Constant matrix
    l : int
        Constraint for A: The values in A must be within :math:`(-2^l, 2^l)`
    tol : float, optional
        The stopping threshold for PPCGM (default is `1e-5`)

    Returns
    -------
    x : (N,) array_like
        The solution to the linear system computed by PPCGM
    """
    Ap_h, Zt, P, T = secure_mask(Ap, l=l)
    x = np.zeros((Ap.shape[0],))
    x_h = T.T @ x

    h0 = Ap_h @ x_h

    r = P.T @ h0 - Zt @ x - bp
    p = -r
    rho = r.T @ r
    delta = (tol * norm(bp))**2
    print("Initial rho: %8f" % rho)
    print("Target rho : %8f" % delta)

    i = 0
    while rho > delta:
        ph1_T = p.T @ P.T
        ph2 = T.T @ p

        f = Ap_h @ ph2
        t = ph1_T @ Ap_h @ ph2

        alpha = rho / (t - p.T @ Zt @ p)
        r = r + alpha * (P.T @ f - Zt @ p)

        rho_old = rho
        rho = r.T @ r

        x = x + alpha * p
        p = -r + (rho / rho_old) * p

        if i < 50 or i % 691 == 0:
            print("iteration {:d} rho {:8f}".format(i, rho))
        i += 1
        if i > 100000:
            break
    print("Exited at iteration {:d} rho {:8f}".format(i, rho))
    return x

def solve(A, b, tol=1e-5):
    """
    Solve any linear system of equations using the proposed algorithm.

    Transform the input `A` and `b` using SLSE_transformation and compute the
    result using PPCGM.

    Parameters
    ----------
    A : (M, N) matrix_like
        Coefficient matrix to the linear system of equations
    b : (M,) matrix_like
        Constant matrix to the linear system of equations
    tol : float, optional
        The stopping threshold for PPCGM (default is `1e-5`)

    Returns
    -------
    x : (N,) array_like
        The solution to the linear system computed by PPCGM
    """
    l = A.max() if A.max() > abs(A.min()) else abs(A.min())
    Ap, bp = slse_transformation(A, b, l=l)
    x = ppcgm(Ap, bp, tol=tol, l=l)
    return x
