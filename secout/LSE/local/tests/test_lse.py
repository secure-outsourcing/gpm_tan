from __future__ import absolute_import

import pytest

pytest.importorskip("numpy")

import numpy as np
import numpy.random as rand
from numpy.testing import assert_allclose, assert_array_almost_equal_nulp, assert_equal
from secout.local.LSE import secure_mask, slse_transformation, ppcgm, solve
from secout.testing import BaseTest

import itertools

ONE_SIMPLE_CASE = (
    np.asarray([
        [1, -1, 3],
        [2, 1, 0],
        [-1, -5, 9]
    ]),
    np.asarray(
        [1, 5, -7]
    )
)

__CHUNKS = [
    np.arange(11, 30),
    np.arange(70, 150),
    np.arange(100, 800),
]

TEST_CASES = list(
    itertools.starmap(
        lambda a, b: (np.outer(a, b) + 1, a),
        itertools.permutations(__CHUNKS, 2)
    )
)

class TestSecureMask(BaseTest):

    @classmethod
    def do(cls, A, **kwargs):
        Ah_0, Zt_0, Q_0_T, P_0_T = secure_mask(A, **kwargs)
        recA = Q_0_T.T @ Ah_0 @ P_0_T.T - Zt_0
        assert_allclose(recA, A)

    def test_1(self):
        A, _ = ONE_SIMPLE_CASE
        TestSecureMask.do(A)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1,1),
            (20, 25),
            (70, 10),
            (90, 200)
        ]
    )
    @pytest.mark.parametrize(
        'exp,l',
        [
            (8, 16),
            (16, 16),
            (32, 16)
        ]
    )
    def test_mask_random(self, m,n, exp, l):
        A = rand.random((m,n)) * 2.**exp - 2.**(exp - 1)
        TestSecureMask.do(A, l=l)


class TestSecureMaskG(BaseTest):

    @classmethod
    def do(cls, A, **kwargs):
        Ah_0, Zt_0, Q_0_T, P_0_T = secure_mask(A, **kwargs)
        Ah_1, Zt_1, _, T_0 = secure_mask(A, Q=Q_0_T, **kwargs)

        G = Ah_0.T @ Ah_1

        M = Zt_0.T @ A + A.T @ Zt_1 + Zt_0.T @ Zt_1
        recG = P_0_T.T @ (A.T @ A + M) @ T_0
        assert_allclose(recG, G)

    def test_1(self):
        A, _ = ONE_SIMPLE_CASE
        TestSecureMaskG.do(A)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1,1),
            (20, 25),
            (70, 10),
            (90, 200)
        ]
    )
    @pytest.mark.parametrize(
        'exp,l',
        [
            (8, 16),
            (16, 16),
            (32, 16)
        ]
    )
    def test_random(self, m,n,exp,l):
        A = rand.random((m,n)) * 2.**exp - 2.**(exp - 1)
        TestSecureMaskG.do(A, l=l)

class TestSecureMaskGp(BaseTest):
    @classmethod
    def do(cls, A, **kwargs):
        Ah_0, Zt_0, Q_0_T, P_0_T = secure_mask(A, **kwargs)
        Ah_1, Zt_1, _, T_0 = secure_mask(A, Q=Q_0_T, **kwargs)

        G = Ah_0.T @ Ah_1

        M = Zt_0.T @ A + A.T @ Zt_1 + Zt_0.T @ Zt_1

        Gp = P_0_T @ G @ T_0.T
        recGp = A.T @ A + M

        assert_allclose(recGp, Gp)

    def test_1(self):
        A, _ = ONE_SIMPLE_CASE
        TestSecureMaskGp.do(A)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1,1),
            (20, 25),
            (70, 10),
            (90, 200)
        ]
    )
    @pytest.mark.parametrize(
        'exp,l',
        [
            (4, 8),
            (8, 8),
            (16, 8)
        ]
    )
    def test_random(self, m,n,exp,l):
        A = rand.random((m,n)) * 2.**exp - 2.**(exp - 1)
        TestSecureMaskGp.do(A, l=l)


class TestSecureMaskATA(BaseTest):
    @classmethod
    def do(cls, A, **kwargs):
        Ah_0, Zt_0, Q_0_T, P_0_T = secure_mask(A, **kwargs)
        Ah_1, Zt_1, _, T_0 = secure_mask(A, Q=Q_0_T, **kwargs)

        G = Ah_0.T @ Ah_1

        M = Zt_0.T @ A + A.T @ Zt_1 + Zt_0.T @ Zt_1

        Gp = P_0_T @ G @ T_0.T

        Ap = A.T @ A

        print(M, M.dtype)

        print(Gp, Gp.dtype)
        print(Ap + M)

        print(Ap, Ap.dtype)
        print(Gp - M)

        print(Gp - M - Ap)
        print(Ap + M - Gp)

        print(Gp - M - Ap + (Ap + M - Gp))

        assert_allclose(Ap + M, Gp)
        assert_allclose(Gp - M, Ap)

    def test_1(self):
        A, _ = ONE_SIMPLE_CASE
        TestSecureMaskATA.do(A)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1, 1),
            (5, 5),
            (5, 5),
            (8, 8)
        ]
    )
    @pytest.mark.parametrize(
        'exp,l',
        [
            (2, 2),
            (4, 2),
            (4, 8),
            (8, 8),
            (16, 16)
        ]
    )
    def test_random(self, m,n,exp,l):
        A = rand.random((m,n)) * 2.**exp - 2.**(exp - 1)
        TestSecureMaskATA.do(A, l=l)

class TestSLSETransformation(BaseTest):
    @classmethod
    def do(cls, A, b, **kwargs):
        Ap, bp = slse_transformation(A, b)

        assert_allclose(Ap, A.T @ A)
        assert_allclose(bp, A.T @ b)

    def test_1(self):
        A, b = ONE_SIMPLE_CASE
        self.__class__.do(A, b)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1, 1),
            (5, 5),
            (5, 5),
            (8, 8)
        ]
    )
    @pytest.mark.parametrize(
        'exp,l',
        [
            (8, 8),
            (16, 16)
        ]
    )
    def test_random(self, m,n,exp,l):
        A = rand.random((m,n)) * 2.**exp - 2.**(exp - 1)
        b = rand.random((m, )) * 2.**(exp - 1)
        self.__class__.do(A, b, l=l)


class TestPPCGM(BaseTest):
    @classmethod
    def do(cls, A, b):
        Ap, bp = A.T @ A, A.T @ b
        x = ppcgm(Ap, bp, tol=1e-5)
        assert_allclose(A @ x, b, atol=1e-3)

    def test_1(self):
        A, b = ONE_SIMPLE_CASE
        self.__class__.do(A, b)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1, 1),
            (5, 5),
            (5, 5),
            (8, 8)
        ]
    )
    @pytest.mark.parametrize(
        'exp',
        [
            8,
            16
        ]
    )
    def test_random(self, m, n, exp):
        A = rand.random((m, n)) * 2. ** exp - 2. ** (exp - 1)
        b = rand.random((m,)) * 2. ** (exp - 1)
        self.__class__.do(A, b)

class TestSolve(BaseTest):
    @classmethod
    def do(cls, A, b):
        x = solve(A, b)
        assert_allclose(A @ x, b, atol=1e-3)

    def test_1(self):
        A, b = ONE_SIMPLE_CASE
        self.__class__.do(A, b)

    @pytest.mark.parametrize(
        'm,n',
        [
            (1, 1),
            (5, 5),
            (5, 5),
            (8, 8)
        ]
    )
    @pytest.mark.parametrize(
        'exp',
        [
            8,
            16
        ]
    )
    def test_random(self, m, n, exp):
        A = rand.random((m, n)) * 2. ** exp - 2. ** (exp - 1)
        b = rand.random((m,)) * 2. ** (exp - 1)
        self.__class__.do(A, b)
