from dask.distributed import Client
import secout.LSE as lse
import numpy as np

n = np.array([[-2, 1, -1], [-1, 2, 1], [2, -1, 4]])

b = np.array([1,0,0])

client = Client(processes=False)
securelse = lse.DaskLSE(n,b,remote=client,local=client)

securelse.solve()

x = securelse.x.compute()

print(x)
print(np.linalg.inv(n) @ b)

client.close()
    
# print(f"res: {type(securelse.x)}")