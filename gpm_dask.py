#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Implementation of GPM operation.
'''

__author__ = 'Tan Li'

import misc
import numpy as np
from dask import delayed


class gpm_dask(object):
    '''
    Attributes:
            f (function): objective function, goal is to minimize it.
            gs (array_like): a list of inequality constaints functions, some of them could become active
            bs (array_like): a list of real number bounds for gs.
            hs (array_like): a list of equality constaints functions, they are active at all time
            ds (array_like): a list of real number bounds for hs.
            x (numpy.ndarray): a vector represents a point, will be updated at each iteration in self.calculate().
            client(dask.distributed.Client): a dask client
            activeGs (array_like): a list of indices of active inequality constaints functions in self.gs.
            activeBs (array_like): a list of indices of real number bounds for active constraints in self.bs.
            accuracy (float): when a number is smaller than accuracy we consider it's close enough to zero
            activeThreshold (float): when the point is within this threshold to a constraint bound, consider this contraint active
    '''

    def __init__(self, f, gs, bs, hs, ds, x0, client, accuracy=0.001, activeThreshold=0.05):
        '''Constructor of GPM.

        Args:
            f (function): objective function, goal is to minimize it.
            gs (array_like): a list of inequality constaints functions.
            bs (array_like): a list of real number bounds for gs.
            hs (array_like): a list of equality constaints functions.
            ds (array_like): a list of real number bounds for hs.
            x0 (numpy.ndarray): initial guess, a vector represents a point, must satisfies all equality constaints.
            client(dask.distributed.Client): a dask client
            accuracy (float): when a number is smaller than accuracy we consider it's close enough to zero
            activeThreshold (float): when the point is within this threshold to a constraint bound, consider this contraint active

        Raises:
            valueError: If the number of constraints and constraint bounds don't match or initial guess is not legal.
        '''

        self.f = f
        self.gs = gs
        self.bs = bs
        self.hs = hs
        self.ds = ds
        self.x = x0
        self.accuracy = accuracy
        self.activeThreshold = activeThreshold
        self.client = client

        # check Args:, raise valueError if not pass
        if self.checkArgs() == False:
            print("initialization stopped.")
            raise ValueError("not a good start.")

        print("initialization finished.")

        # active constraints are to be determinted
        self.activeGs = []
        self.activeBs = []

    def checkArgs(self):
        '''Check if all args are legal.

        Returns:
            bool: True if all legal, False otherwise.
        '''

        if len(self.gs) != len(self.bs) and not self.gs and not self.bs:
            print('number of inequality constraints and bounds don\'t match.')
            print(f"{len(self.gs)},{len(self.bs)}")
            return False

        if len(self.hs) != len(self.ds) and not self.hs and not self.ds:
            print('number of equality constraints and bounds don\'t match.')
            print(f"{len(self.hs)},{len(self.ds)}")
            return False

        for h, d in zip(self.hs, self.ds):
            if h(*self.x) > d or d - h(*self.x) > self.accuracy:
                print(
                    f"initial guess doesn't satisfy equality constraints: {h(*self.x)}, {h}")
                return False

        for g, b in zip(self.gs, self.bs):
            if g(*self.x) > b:
                print(
                    f"initial guess doesn't satisfy inequality constraints: {g(*self.x)}, {g}")
                return False

        return True

    def updateActive(self, x):
        '''Update active constraints based on given point x.
        Args:
            x (numpy.ndarray): a vector represents a point.
        '''
        self.activeGs = []
        self.activeBs = []

        for i in range(len(self.gs)):
            if self.bs[i] - self.gs[i](*x) <= self.activeThreshold:
                self.activeGs.append(i)
                self.activeBs.append(i)
        print(
            f"{len(self.activeGs)} active inequality constraints out of {len(self.gs)}")

    def ifViolate(self, x):
        for i in range(len(self.gs)):
            if self.gs[i](*x) - self.bs[i] > 0:
                return True

        for i in range(len(self.hs)):
            if self.hs[i](*x) - self.ds[i] > 0 or self.ds[i] - self.hs[i](*x) > self.accuracy:
                return True

        return False

    def getGx(self, x):
        '''Get the gap between the values of active constraint functions and the corresponding constants.

        Args:
            x (numpy.ndarray): a vector represents a point.
        '''
        __res = []
        for i in self.activeGs:
            __res.append(self.gs[i](*x)-self.bs[i])

        for h, d in zip(self.hs, self.ds):
            __res.append(h(*x)-d)

        return np.array(__res)

    def calculate(self, reduction=0.05):
        '''start gpm loop, final result saved to self.x

        Args:
            reduction (float)): the desired specified reduction, reduction ~=( f(Xk)-f(Xk+1) )/ f(Xk)
        '''

        numOfVariables = len(self.x)

        x = self.x
        update = True
        iteration = 0
        while iteration < 3000:
            iteration += 1
            print(f"iteration {iteration}:")
            print(f"x: {x}")
            print(f"f(x): {self.f(*x)}")

            if update:
                self.updateActive(x)
            else:
                update = not update

            gradientF = misc.gradient(self.f, x)
            # print(f"gradientF:\n{gradientF}")

            gradientOfConstraints = []

            for i in range(len(self.activeGs)):
                gradientOfConstraints.append(
                    misc.gradient(self.gs[self.activeGs[i]], x))

            for i in range(len(self.hs)):
                gradientOfConstraints.append(misc.gradient(self.hs[i], x))

            # print(f"self.gs[0](*x): {self.gs[0](*x)}")
            # print(f"self.gs[self.activeGs[0]](*x): {self.gs[self.activeGs[0]](*x)}")
            # print(f"first gradient of active constraints:\n{misc.gradient(self.gs[0], x)}")
            # print(f"gradientOfConstraints:\n{gradientOfConstraints}")
            # N is the gradient matrix of active constraints

            if gradientOfConstraints:
                N = np.column_stack(tuple(gradientOfConstraints)).T
                # P is the projection matrix
                
                sec_n_nt = misc.secureMultiplication(N,self.client)
                sec_n_nt_inv = misc.secureInverse(sec_n_nt,self.client)

                print(f"sec_n_nt: {sec_n_nt}")
                print(f"sec_n_nt expected: {N @ N.T}")
                
                print(f"sec_n_nt_inv: {sec_n_nt_inv}")
                print(f"sec_n_nt_inv expected: {np.linalg.inv(N @ N.T)}")
                P = np.identity(numOfVariables) - \
                    N.T @ sec_n_nt_inv @ N
                # print(f"P: {P}")
                # print(f"P expected: {np.identity(numOfVariables) - N.T @ np.linalg.inv(N @ N.T) @ N}")
            else:
                N = np.zeros((1, numOfVariables))
                P = np.identity(numOfVariables)

            # print(f"N:\n{N}")
            # print(f"P:\n{P}")
            # print(f"N*N.T:\n{N@N.T}")

            # S is the search direction
            S = np.negative(P) @ gradientF
            # S = gradientF
            print(f"S: {S}")
            if np.linalg.norm(S) > self.accuracy:
                print(f"norm(S): {np.linalg.norm(S)}")
                # gx is the gap between the value of constraint functions and bounds
                if gradientOfConstraints:
                    gx = self.getGx(x)
                    # print(f"g(X): {gx}")
                    _x = x - (reduction * self.f(*x) / np.dot(S, gradientF)) * S + \
                        np.dot(N.T @ sec_n_nt_inv, gx)

                    # print(f"b1: {-(reduction * self.f(*x) / np.dot(S, gradientF)) * S}")
                    # print(f"alfa: {(reduction * self.f(*x) / np.dot(S, gradientF))}")
                    # print(f"b2: {np.dot(N.T @ np.linalg.inv(N @ N.T), gx)}")

                    # check if new x is out of constrainted space, if so set reduction to half until new x is valid
                    _reduction = reduction
                    while self.ifViolate(_x):
                        print(f"_x: {_x}")
                        _reduction = 0.5 * _reduction
                        _x = x - (_reduction * self.f(*x) / np.dot(S, gradientF)) * S + \
                            np.dot(N.T @ sec_n_nt_inv, gx)
                    x = _x
                    # print(f"np.dot(N.T @ np.linalg.inv(N @ N.T), gx):\n{np.dot(N.T @ np.linalg.inv(N @ N.T), gx)}")
                    # gx = self.getGx(x)
                    # while np.linalg.norm(gx) > self.accuracy:
                    #     print(f"gx : {gx}")
                    #     x = x - np.dot(N.T @ np.linalg.inv(N @ N.T), gx)
                    #     gx = self.getGx(x)
                else:
                    _x = x - (reduction * self.f(*x) / np.dot(S, gradientF)) * S
                    print(
                        f"alfa: {(reduction * self.f(*x) / np.dot(S, gradientF))}")
                    print(
                        f"b1: {-(reduction * self.f(*x) / np.dot(S, gradientF)) * S}")
                    _reduction = reduction
                    while self.ifViolate(_x):
                        print(f"_x: {_x}")
                        _reduction = 0.5 * _reduction
                        _x = x - (_reduction * self.f(*x) /
                                  np.dot(S, gradientF)) * S
                    x = _x

                # print(f"alfa : {reduction * self.f(*x) / np.dot(S, gradientF)}")
                # print(f"N.T @ np.linalg.inv(N @ N.T):\n{N.T @ np.linalg.inv(N @ N.T)}")
                # print(f"gx:\n{gx}")
                # print(f"np.dot(N.T @ np.linalg.inv(N @ N.T), gx):\n{np.dot(N.T @ np.linalg.inv(N @ N.T), gx)}")
            else:
                # compute Lagrange multipliers to check if x is optimal
                gx = self.getGx(x)
                # L = -np.linalg.inv(N @ N.T) @ N @ gradientF - (np.dot(S, gradientF) / (reduction * self.f(*x))) * np.dot(np.linalg.inv(N @ N.T), gx)
                L = -np.linalg.inv(N @ N.T) @ N @ gradientF
                print(f"Lagrange multipliers: {L}")
                LMin = np.amin(L)
                print(f"Min of Lagrange multipliers: {LMin}")
                if LMin >= 0:
                    break
                else:
                    update = False
                    LMinIndex = np.where(L == LMin)[0][0]
                    print(f"LMinIndex: {LMinIndex}")
                    if LMinIndex < len(self.activeGs):
                        print(
                            f"Number {self.activeGs[LMinIndex]} active inequality constraints is removed.")
                        self.activeGs.pop(LMinIndex)
                        self.activeBs.pop(LMinIndex)
                    else:
                        print(
                            f"Number {LMinIndex - len(self.activeGs)} equality constraints is removed.")
                        self.hs.pop(LMinIndex - len(self.activeGs))
                        self.ds.pop(LMinIndex - len(self.activeGs))

        print(f"final result x* : {x}, f(x*) : {self.f(*x)}")
        self.x = x

        # print(f"{self.f(*np.array([0,1,2,-1]))}")
