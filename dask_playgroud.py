from dask.distributed import Client
from dask import delayed

from time import sleep
# client = Client('192.168.1.105:8786')
client = Client(processes = False)
def inc(x):
    sleep(1)
    return x + 1

def add(x, y):
    sleep(1)
    return x + y
data = [1, 2, 3, 4, 5, 6, 7, 8]
results = []
for x in data:
    y =delayed(inc)(x)
    # y = inc(x)
    results.append(y)
    
total = delayed(sum)(results)
# res = client.submit(sum,results)
res = client.compute(total)
print(res.result())
# print(total.compute())
# print(total)
# print(client.gather(res))