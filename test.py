#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''test gpm module.
'''

__author__ = 'Tan Li'

from gpm import gpm
import numpy as np

# Testv 1
# objective function


def test1():
    def f(x0, x1, x2, x3):
        '''Rosen-Suzuki Problem
        f(x) = x_1^2 + x_2^2 + 2x_3^2 + x_4^2 - 5x_1 - 5x_2 - 21x_3 + 7x_4
        The initial point for the optimization procedure is x=(1,1,1,1)
        and the optimal point is x*=(0,1,2,-1)
        with an optimal function value of f(x*) = -44.
        '''
        return x0*x0 + x1 * x1 + 2 * x2 * x2 + x3 * x3 - 5 * x0 - 5 * x1 - 21 * x2 + 7 * x3 + 45

    # below are inequality constraints

    def g0(x0, x1, x2, x3):
        return -(8 - x0*x0 - x1*x1 - x2*x2 - x3*x3 - x0 + x1 - x2 + x3)

    def g1(x0, x1, x2, x3):
        return -(10 - x0*x0 - 2*x1*x1 - x2*x2 - 2*x3*x3 + x0 + x3)

    def g2(x0, x1, x2, x3):
        return -(5 - 2*x0*x0 - x1*x1 - x2*x2 - 2*x0 + x1 + x3)

    gs = [g0, g1, g2]
    bs = [0, 0, 0]

    # no equality constraints, use empty list
    hs = []
    ds = []

    # x0 is initial point
    x0 = np.array([0.01, 0.99, 1.99, -1])

    g = gpm(f, gs, bs, hs, ds, x0)
    g.calculate(0.2)
    expected = [0, 1, 2, -1]
    print(f"expected result x*: {expected}, f(x*) : {f(*expected)}")

# Test2
# objective function, see pdf ConstraintOpt.pdf at Chapter 5, Example 5.5.1 at page 22.


def test2():
    def f1(x1, x2, x3, x4): return x1**2+x2**2+x3**2+x4**2-2*x1-3*x4

    # constaints

    def _g0(x1, x2, x3, x4): return -(2*x1+x2+x3+4*x4-7)

    def _g1(x1, x2, x3, x4): return -(x1+x2+x3**2+x4-5.1)

    def _g2(x1, x2, x3, x4): return -x1

    def _g3(x1, x2, x3, x4): return -x2

    def _g4(x1, x2, x3, x4): return -x3

    def _g5(x1, x2, x3, x4): return -x4

    gs = [_g0, _g1, _g2, _g3, _g4, _g5]
    bs = [0, 0, 0, 0, 0, 0]

    # no equality constraints, use empty list
    hs = []
    ds = []

    # x0 is initial point
    x0 = np.array([2, 3, 1, 0])
    x0 = np.array([1.49509566, 1.65682198, 0.42641429, 1.77927059])

    g = gpm(f1, gs, bs, hs, ds, x0)
    g.calculate(0.1)
    expected = [0, 1, 2, -1]
    print(f"expected result x*: {expected}, f(x*) : {f(*expected)}")

# Test3


def test3():
    def f1(x1, x2): return 3*x1+3**0.5*x2

    # constaints

    def _g0(x1, x2): return -3+18/x1+6*(3**0.5)/x2

    def _g1(x1, x2): return -x1+5.73

    def _g2(x1, x2): return -x2+7.17

    gs = [_g0, _g1, _g2]
    bs = [0, 0, 0]

    # no equality constraints, use empty list
    hs = []
    ds = []

    # x0 is initial point
    x0 = np.array([11, 10])

    g = gpm(f1, gs, bs, hs, ds, x0)
    print(f"g0: {g.gs[0](*x0)}")
    g.calculate(0.05)
    expected = [9.464, 9.464]
    print(f"expected result x*: {expected}, f(x*) : {f1(*expected)}")


def test4():
    def f(x1, x2, x3): return x1**2+x2**2+x3**2+x1+x2+x1*x2

    def g0(x1, x2, x3): return -(x1+x2)

    def g1(x1, x2, x3): return -x2

    def g2(x1, x2, x3): return -x3

    gs = [g0, g1, g2]
    bs = [-3, -2, -3]
    # no equality constraints, use empty list
    hs = []
    ds = []

    x0 = np.array([20, 3, 4])

    g = gpm(f, gs, bs, hs, ds, x0)
    g.calculate(0.05)
    expected = [1, 2, 3]
    print(f"expected result x*: {expected}, f(x*) : {f(*expected)}")


def test5():
    def f(x0, x1):
        '''
        objective function
        '''
        return -(143*x0 + 60*x1)

    '''
    inequality constraints
    '''
    def g0(x0, x1):
        return 15000 - 120 * x0 - 210 * x1

    def g1(x0, x1):
        return 4000 - 110 * x0 - 30 * x1

    def g2(x0, x1):
        return 75 - x0 - x1

    gs = [g0, g1, g2]
    bs = [0, 0, 0]

    # no equality constraints, use empty list
    hs = []
    ds = []

    # initial point is 20,30
    x0 = np.array([20, 30])
    g = gpm(f, gs, bs, hs, ds, x0)
    g.calculate()
    expected = [21.88, 53.12]
    print(f"expected x*: {expected}, f(x*) : {f(*expected)}")


def test6():
    def f(x0, x1):
        '''
        Himmelblau's function is a multi-modal function, (possibly wrong here)
        used to test the performance of optimization algorithms.
        '''
        return (x0 ** 2 + x1 - 11) ** 2 + (x0 + x1 ** 2 - 7) ** 2

    gs = []
    bs = []
    # no equality constraints, use empty list
    hs = []
    ds = []

    # initial point
    x0 = np.array([-0.270845, -0.923039])
    g = gpm(f, gs, bs, hs, ds, x0)
    g.calculate()
    expected = [3, 2]
    print(f"expected x*: {expected}, f(x*) : {f(*expected)}")


def test7():
    def f(x1, x2):
        '''

        objective function
        '''
        return 150*x1 + 175*x2

    '''
    inequality constraints
    '''

    def g0(x1, x2):
        return 77 - 7*x1 - 7*x2

    def g1(x1, x2):
        return 80 - 10 * x1 - 8 * x2

    def g2(x1, x2):
        return 9 - x1

    def g3(x1, x2):
        return 6 - x2

    gs = [g0, g1, g2, g3]
    bs = [0, 0, 0, 0]

    # no equality constraints, use empty list
    hs = []
    ds = []

    # initial point is 20,30
    x0 = np.array([5, 4])
    g = gpm(f, gs, bs, hs, ds, x0)
    g.calculate()
    expected = [4.9, 3.9]
    print(f"expected x*: {expected}, f(x*) : {f(*expected)}")


if __name__ == '__main__':
    # test1()
    # test2()
    test3()
    # test4() 
    # test5()
    # test7()
