import numpy as np

y = np.array([0,0,1])
y = y.reshape((3,1))
print(f"y:\n{y}")

# n = np.array([[-2,1,-1],[-1,2,1],[2,-1,4]])
n = np.array([[1, 3, 3], [1, 4, 3], [1, 3, 4]])
print(f"n:\n{n}")

initial = np.array([0,0,0])
initial = initial.reshape((3,1))
print(f"initial:\n{initial}")

D = np.diag(np.diag(n))
E = n - D
D_inv = np.linalg.inv(D)

print(f"D:\n{D}")
print(f"E:\n{E}")

print(f"egin:\n{np.linalg.eig(D_inv @ E)}")

# x0 = -D_inv @ E @ initial + D_inv @ y
# print(x0)

# count = 0
# norm = 10
# while norm > 0.0001:
#     x1 = -D_inv @ E @ x0 + D_inv @ y
#     norm = np.linalg.norm(x1-x0)
#     x0 = x1
#     count += 1
# print(x0)