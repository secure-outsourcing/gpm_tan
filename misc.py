#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Help functions for gpm.py to use.
'''

__author__ = 'Tan Li'

from scipy import optimize
from autoDifferentiation import gradient as autoGrad
from dask import delayed
from dask.distributed import Client
import dask.array as da
import numpy as np
import secout.LSE as lse


def gradient(f, x):
    '''return gradient of function f at point x

    Args:
        f (function):  a function
        x (numpy.ndarray):  a vector

    Returns:
        numpy.ndarray: the gradient of f at x
    '''
    # print(eps)
    return np.array(autoGrad(f, x))


def secureMultiplication(n, remote_client):
    '''return n@n.T using secure outsourcing methods

    Args:
        n (np.ndarray): a matrix
        remote_client (dask.distributed.Client): a remote dask client

    Returns:
        numpy array: the result of n @ n.T
    '''
    (i, j) = n.shape
    N = da.from_array(n)
    r = np.random.rand(i, j)
    R = da.from_array(r)
    Nk = N + R
    Ok = Nk @ Nk.T
    Tk = n @ r.T + r @ n.T + r @ r.T

    Ok_res = remote_client.compute(Ok).result()

    res = Ok_res - Tk

    return res


def secureInverse(n, remote_client):
    '''return inverse of n using secure outsourcing methods

    Args:
        n (np.ndarray): a matrix
        local_client (dask.distributed.Client): a local dask client
        remote_client (dask.distributed.Client): a remote dask client

    Returns:
        numpy array: the result of inverse(n)
    '''
    (i, j) = n.shape
    eye = np.eye(i)
    res = []
    if i == 1: return np.linalg.inv(n)
    for row in eye:
        print(f"row: \n{row}")
        securelse = lse.DaskLSE(n,row,remote=remote_client,local=remote_client)

        securelse.solve()

        x = securelse.x.compute()

        res.append(x)
    
    return np.column_stack(res)


if __name__ == '__main__':
    # def f(x):
    #     '''Rosen-Suzuki Problem
    #     f(x) = x_1^2 + x_2^2 + 2x_3^2 + x_4^2 - 5x_1 - 5x_2 - 21x_3 + 7x_4
    #     The initial point for the optimization procedure is x=(1,1,1,1)
    #     and the optimal point is x*=(0,1,2,-1)
    #     with an optimal function value of f(x*) = -44.
    #     '''
    #     return x[0]**2 + x[1]**2 + 2 * x[2]**2 + x[3]**2 - 5 * x[0] - 5 * x[1] - 21 * x[2] + 7 * x[3]

    # def f(x0, x1, x2, x3): return x0**2 + x1**2 + 2 * \
    #     x2**2 + x3**2 - 5 * x0 - 5 * x1 - 21 * x2 + 7 * x3

    # x = np.array([1, 1, 1, 1])

    # # should be [-3,-3,-17,9]
    # print(gradient(f, x))
    # print(f(*x))

    # a = np.array([[1,2],[3,4]])
    # b = np.array([8,9])
    # print(a)
    # print(b)
    # print(np.dot(a,b))
    client = Client(processes=False)
    # n = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
    n = np.array([[-2, 1, -1], [-1, 2, 1], [2, -1, 4]])
    # res = secureMultiplication(n, client)
    print(n @ n.T)
    # print(res)


    res = secureInverse(n,client)

    
    print(np.linalg.inv(n))
    print(res)
    print(res @ n)

    client.close()
